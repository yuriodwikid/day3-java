package comp;

import java.util.Scanner;

public class tugas2 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Baris Array integer = ");
        int a = input.nextInt();
        System.out.print("Baris Arrat integer = ");
        int b = input.nextInt();

        int[][] index = new int[a][b];

        for (int i = 0; i < index.length; i++) {
            for (int j = 0; j < index[i].length; j++) {
                System.out.print("Index [" + i + "][" + j + "]: ");
                index[i][j] = input.nextInt();
            }
        }
        System.out.println("\nBaris dan Kolom Array 2 Dimensi adalah [" + a + "][" + b + "]");
        System.out.print("Array Multidimensi 2 Dimensi\n");
        for (int i = 0; i < index.length; ++i) {
            for (int j = 0; j < index[i].length; ++j) {
                System.out.print(index[i][j] + " ");
            }
            System.out.println();
        }
    }

}
