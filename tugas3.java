package comp;

import java.util.Scanner;
import java.util.Arrays;

public class tugas3 {
    public static void bubbleSort(int[]a){

        for (int i = 0; i < a.length - 1; i++) {
            for (int j = 0; j < a.length - 1 - i; j++) {

                if (a[j + 1] < a[j]) {
                    int temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                }
            }
        }
    }
    public static void BinarySearch (int[]a, int target){

            int left = 0;
            int middle;
            int right = a.length - 1;
            while (left <= right) {
                middle = (left + right) / 2;
                if (a[middle] == target) {
                    System.out.println("Element found at index " + middle);
                    break;
                } else if (a[middle] < target) {
                    left = middle + 1;
                } else if (a[middle] > target) {
                    right = middle - 1;
                }
            }

        }

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.print("Panjang Array Integer: ");
        int inputArr = input.nextInt();

        int [] index = new int[inputArr];

        int pilihan;

        do {
            System.out.println("##  Daftar Menu##");
            System.out.println("============================");
            System.out.println("1. Input data Array");
            System.out.println("2. Sortir Data");
            System.out.println("3. Search Data");
            System.out.println("4. Exit");
            System.out.println();

            System.out.print("Pilihan anda: ");
            pilihan = input.nextInt();



            switch (pilihan) {
                case 1:
                    System.out.print("Banyak data yang dimasukan...\n ");
                        for (int i = 0 ; i < index.length; i++) {
                            System.out.println("masukan angka index"+i+": ");
                            index[i] = input.nextInt();
                        }

                    System.out.println("Array Integer ");
                        for (int item: index) {
                            System.out.print(" "+item+"");
                        }
                    System.out.println("\n");
                        break;
                case 2:
                    bubbleSort(index);
                    System.out.println("Bubble sort");
                    for (int item: index) {
                        System.out.print(" "+item+" ");
                    }
                    System.out.println("\n");
                    break;
                case 3:
                    System.out.println("yang di cari");
                    int inputTujuan = input.nextInt();
                    BinarySearch (index, inputTujuan);
                    break;

                default:

            }

            System.out.println();

//            System.out.print("Ingin memilih menu lain (y/t)? ");
//            ulang = input.next().charAt(0);

            System.out.println();
        }
        while (pilihan < 3);

        System.out.println("Exit");

    }
}


